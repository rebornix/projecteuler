vect = [[0 for col in range(21)] for row in range(21)]

def cal(i, j):
    if vect[i][j] == 0:
        if i > 0 and j > 0:
            vect[i][j] = cal(i-1,j) + cal(i, j-1)
        elif i > 0:
            vect[i][j] = cal(i-1,j)
        elif j > 0:
            vect[i][j] = cal(i, j-1)
        else:
            vect[i][j] = 1
    return vect[i][j]

print cal(20,20)
