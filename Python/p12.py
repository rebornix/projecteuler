primeList = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 47, 53]

def Count(n):
    result = 1;
    for item in primeList:
        count = 0
        while n % item is 0:
            count += 1
            n = n / item
        if count is not 0:
            result = result * ( count + 1)
        if n == 1:
            break
    return result

n = 2
print Count(28)
while(True):
    a = 0
    b = 0
    if n % 2 == 0:
        a = n / 2
        b = n + 1
    else:
        a = (n + 1)/2
        b = n
    if Count(a) * Count(b) >= 500:
        print a, b,
        print a * b
        break
    else:
        n = n + 1
